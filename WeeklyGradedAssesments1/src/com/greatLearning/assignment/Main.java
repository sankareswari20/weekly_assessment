package com.greatLearning.assignment;

public class Main
{

	public static void main(String[] args)
	{
		// create the object of All Department and use all the methods
		
		HrDepartment hrdept = new HrDepartment();
		AdminDepartment admindept = new AdminDepartment();
		TechDepartment techdept = new TechDepartment();
		SuperDepartment superdept = new SuperDepartment();	
		
		System.out.println(admindept.departmentName());
		System.out.println(admindept.getTodaysWork());
		System.out.println(admindept.getWorkDeadline());
		System.out.println(admindept.isTodayHoliday());		
		System.out.println();		
		
		System.out.println(hrdept.departmentName());
		System.out.println(hrdept.getTodaysWork());
		System.out.println(hrdept.getWorkDeadline());
		System.out.println(hrdept.doActivity());
		System.out.println(hrdept.isTodayHoliday());	 
		System.out.println();
		
		System.out.println(techdept.departmentName());
		System.out.println(techdept.getTodaysWork());
		System.out.println(techdept.getWorkDeadline());
		System.out.println(techdept.getTechStackInformation());
		System.out.println(techdept.isTodayHoliday()); 
		System.out.println();
		
		System.out.println(superdept.departmentName());
		System.out.println(superdept.getTodaysWork());
		System.out.println(superdept.getWorkDeadline());
		System.out.println(superdept.isTodayHoliday());
		System.out.println();
	}

}
