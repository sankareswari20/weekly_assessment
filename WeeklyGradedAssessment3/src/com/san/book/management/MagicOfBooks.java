package com.san.book.management;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

	public class MagicOfBooks {
			Scanner scanner=new Scanner(System.in);
			HashMap<Integer,BookClass> bookmap=new HashMap<>();
			TreeMap<Double,BookClass> treemap=new TreeMap<>();
			ArrayList<BookClass> booklist=new ArrayList<>();
			public  void addbook(){

				BookClass book= new BookClass();
				System.out.println("Please Enter book id : ");
				book.setId(scanner.nextInt());

				System.out.println(" Please Enter book name : ");
				book.setName(scanner.next());

				System.out.println(" Please Enter book price : ");
				book.setPrice(scanner.nextDouble());

				System.out.println("Enter book genre (Autobiography-Autobiography) : ");
				book.setGenre(scanner.next());

				System.out.println("Enter number of copies sold : ");
				book.setNoOfCopiesSold(scanner.nextInt());

				System.out.println("Enter book status (B-bestselling) : ");
				book.setBookstatus(scanner.next());

				bookmap.put(book.getId(), book);
				System.out.println("Book added successfully");

				treemap.put(book.getPrice(), book);
				booklist.add(book);

			}
			public void deletebook() throws BookCustomerException{
				if(bookmap.isEmpty()) {
					throw new BookCustomerException("no books are available to delete!!");
				}
				else {
					System.out.println("Enter book id you want to delete : ");
					int id=scanner.nextInt();
					bookmap.remove(id);	
					System.out.println("successfully deleted!!");
				}


			}
			public void updatebook() throws BookCustomerException {
				if(bookmap.isEmpty()) {
					throw new BookCustomerException("No books are available to update!!");
				}else {


					BookClass b= new BookClass();
					System.out.println("Please  Enter book id : ");
					b.setId(scanner.nextInt());

					System.out.println("Please Enter book name : ");
					b.setName(scanner.next());

					System.out.println(" Please Enter book price : ");
					b.setPrice(scanner.nextDouble());

					System.out.println("Please Enter book genre : ");
					b.setGenre(scanner.next());

					System.out.println("Enter number of copies sold : ");
					b.setNoOfCopiesSold(scanner.nextInt());

					System.out.println("Enter book status : ");
					b.setBookstatus(scanner.next());

					bookmap.replace(b.getId(), b);
					System.out.println("Book details Updated successfully!!");
				}
			}
			public void displayBookInfo() throws BookCustomerException {

				if (bookmap.size() > 0) 
				{
					Set<Integer> keySet = bookmap.keySet();

					for (Integer key : keySet) {

						System.out.println(key + " ----> " + bookmap.get(key));
					}
				} else {
					throw new BookCustomerException("BooksMap is Empty!!");
				}

			}
			public void count() throws BookCustomerException {
				if(bookmap.isEmpty()) {
					throw new BookCustomerException("book store is empty!!");
				}else

					System.out.println("Number of books present in the store : "+bookmap.size());

			}	
			public void autobiography() throws BookCustomerException {
				String bestSelling = "Autobiography";
				if(booklist.isEmpty()) {
					throw new BookCustomerException("Book store is Empty!!");
				}
				else {

					ArrayList<BookClass> genreBookList = new ArrayList<BookClass>();

					Iterator<BookClass> iter=(booklist.iterator());

					while(iter.hasNext())
					{
						BookClass b1=(BookClass)iter.next();
						if(b1.genre.equals(bestSelling)) 
						{
							genreBookList.add(b1);
							System.out.println(genreBookList);
						}
					} 
				}
			}
			public void displayByFeature(int flag) throws BookCustomerException{

				if(flag==1)
				{
					if (bookmap.size() > 0) 
					{
						Set<Double> keySet = treemap.keySet();
						for (Double key : keySet) {
							System.out.println(key + " ----> " + treemap.get(key));
						}
					} else {
						throw new BookCustomerException("book store is empty is Empty!!");
					}
				}
				if(flag==2) 		
				{
					Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
					NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();

					System.out.println("Book Details : ");

					if (nmap.size() > 0) 
					{
						Set<Double> keySet = nmap.keySet();
						for (Double key : keySet) {
							System.out.println(key + " ----> " + nmap.get(key));
						}
					}else{
						throw new BookCustomerException("book store is Empty!!");
					}

				}

				if(flag==3) 
				{
					String bestSelling = "B";
					ArrayList<BookClass> genreBookList = new ArrayList<BookClass>();
					Iterator<BookClass> iter=booklist.iterator();
					while(iter.hasNext())
					{
						BookClass b=(BookClass)iter.next();
						if(b.bookstatus.equals(bestSelling)) 
						{
							genreBookList.add(b);
						}
					}
					if(genreBookList.isEmpty())
					{
						throw new BookCustomerException("no best selling books are available!!");
					}
					else
						System.out.println("best selling books : "+genreBookList);
				}
			}	
		}

