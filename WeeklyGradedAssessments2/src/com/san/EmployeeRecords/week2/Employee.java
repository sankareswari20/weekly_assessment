package com.san.EmployeeRecords.week2;

//create class for Employee 
public class Employee 
{
	// Decalre Employees Variable
	int id;
	String name;
	int age;
	int salary; 
	String department;
	String city;
	

	// Set Value of Employee
	public void setId(int id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public void setSalary(int salary) {
		this.salary = salary;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	// Get the value of Employee
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public int getSalary() {
		return salary;
	}
	public String getDepartment() {
		return department;
	}
	public String getCity() {
		return city;
	}
	
	public Employee(int id, String name, int age, int salary, String department, String city) throws IllegalArgumentException
	{
		super();
		
		this.id = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.department = department;
		this.city = city;
		
	}

	@Override
	public String toString() {
		return "Id=" + id + ", Name=" + name + ", Age=" + age + ", Salary=" + salary + ", Department="
				+ department + " City=" + city ;
	}

	
	
}
