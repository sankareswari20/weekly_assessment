package com.san.EmployeeRecords.week2;

import java.util.ArrayList;
import java.util.Collections;

public class EmployeeNameSorting {

public void sortingNames(ArrayList<Employee> employees) {
		
		Collections.sort(employees,(emp1, emp2)->emp1.getName().compareTo(emp2.getName()));
		
		System.out.println();
        System.out.println("Employee names in the sorted order:  ");
		System.out.print("[");

		for (Employee employee : employees) {
			System.out.print(employee.getName()+",");
		}
		System.out.println("]");



	}

}
